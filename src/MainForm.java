import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainForm 
{
	private JFrame frame;
	private JMapViewer mapa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		mapa = new JMapViewer();
		
		mapa.setZoomContolsVisible(false);
		mapa.setDisplayPositionByLatLon(-34.521, -58.7008, 12);
		
		//Ponemos un marcador
		
		MapMarker marker1 = new MapMarkerDot("aqui",new Coordinate(-34.521, -58.7008));
		marker1.getStyle().setBackColor(Color.BLUE);
		marker1.getStyle().setColor(Color.BLUE);
		
		MapMarker marker2 = new MapMarkerDot(-34.546, -58.719);
		marker2.getStyle().setBackColor(Color.RED);
		marker2.getStyle().setColor(Color.RED);
		
		mapa.addMapMarker(marker1);		
		mapa.addMapMarker(marker2);
		
		//Ponemos un poligono para unir puntitos
		ArrayList<Coordinate> coordenadas = new ArrayList<Coordinate>();
		coordenadas.add(new Coordinate(-34.521, -58.7008));
		coordenadas.add(new Coordinate(-34.546, -58.719));
//		coordenadas.add(new Coordinate(-34.521, -58.7008));
		coordenadas.add(new Coordinate(-34.559, -58.721));
		coordenadas.add(new Coordinate(-34.569, -58.725));
		coordenadas.add(new Coordinate(-34.532, -58.730));
		
		MapPolygon poligono = new MapPolygonImpl(coordenadas);
	
		mapa.addMapPolygon(poligono);
		
		frame.getContentPane().add(mapa);
	}

}
